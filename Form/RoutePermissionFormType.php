<?php

namespace Ascend\RoleMatrix\Form;

use Ascend\RoleMatrix\Entity\Role;
use Knp\Menu\MenuItem;
use Knp\Menu\Provider\MenuProviderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Routing\RouterInterface;

abstract class RoutePermissionFormType extends AbstractType
{

    /** @var RouterInterface */
    protected $router;

    /** @var MenuProviderInterface */
    protected $menu;

    public function __construct(
        RouterInterface $router,
        MenuProviderInterface $menu
    )
    {
        $this->router = $router;
        $this->menu = $menu;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'role',
                EntityType::class,
                [
                    'class' => Role::class,
                    'empty_data' => null,
                    'placeholder' => 'sylius.ui.select',
                    'label' => 'app.ui.role',
                    'required' => true,
                    'attr' => [
                        'class' => 'select2-select'
                    ]
                ]
            )
            ->add(
                'route',
                ChoiceType::class,
                [
                    'choices' => $this->getRoutes(),
                    'label' => 'app.ui.route',
                    'multiple' => true,
                    'required' => true,
                ]
            )
            ->add(
                'menu',
                ChoiceType::class,
                [
                    'choices' => $this->getMenuItems(),
                    'label' => 'app.ui.menu',
                    'multiple' => true,
                    'required' => true,
                ]
            );
    }

    private function getMenuItems(): array
    {
        $menuItems = [];
        $menu = $this->menu->get('sylius.admin.main');
        foreach ($menu->getChildren() as $menuMainChildren) {
            /** @var MenuItem $child */
            foreach ($menuMainChildren as $child) {
                $menuItems[$child->getLabel()] = $child->getName();
            }
        }

        return $menuItems;
    }

    private function getRoutes(): array
    {
        $routes = [];
        foreach ($this->router->getRouteCollection() as $route) {
            $routeDefaults = $route->getDefaults();
            if (isset($routeDefaults["_sylius"])) {
                if (isset($routeDefaults["_sylius"]['section'])) {
                    if ($routeDefaults["_sylius"]['section'] == 'admin') {
                        $routes[$route->getPath()] = $route->getPath();
                    }
                }
            }
        }

        return $routes;
    }
}