<?php

namespace Ascend\RoleMatrix\Form;

use Ascend\RoleMatrix\Entity\Permission;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

abstract class RoleFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'name',
                TextType::class,
                [
                    'label' => 'app.ui.role',
                    'required' => true,
                ]
            )
            ->add(
                'locked',
                HiddenType::class,
                [
                    'empty_data' => false,
                ]
            )
            ->add(
                'permissions',
                EntityType::class, [
                    'class' => Permission::class,
                    'multiple' => true,
                    'expanded' => true,
                    'choice_label' => 'name',
                ]
            );
    }
}