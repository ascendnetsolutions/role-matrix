<?php

namespace Ascend\RoleMatrix\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

abstract class PermissionFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'action',
                TextType::class,
                [
                    'required' => true,
                    'label' => 'app.ui.action_name'
                ]
            )
            ->add(
                'name',
                TextType::class,
                [
                    'required' => true,
                    'label' => 'app.ui.name'
                ]
            )
        ;
    }
}