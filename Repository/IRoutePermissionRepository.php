<?php

namespace Ascend\RoleMatrix\Repository;

interface IRoutePermissionRepository
{
    public function findPermissionForRole(array $role): array;
}