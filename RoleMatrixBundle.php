<?php

declare(strict_types=1);

namespace Ascend\RoleMatrix;

use Symfony\Component\HttpKernel\Bundle\Bundle;

final class RoleMatrixBundle extends Bundle
{
}