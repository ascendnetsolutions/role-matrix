<?php

namespace Ascend\RoleMatrix\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\MappedSuperclass()
 * @UniqueEntity("role")
 */
class RoutePermission
{
    const VIEW = 'view_route';
    const EDIT = 'edit';
    const DELETE = 'delete';
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /** @ORM\Column(type="json", name="route") */
    private $route;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Role")
     * @ORM\JoinColumn(name="role_id", referencedColumnName="id")
     * @Assert\NotBlank()
     */
    private $role;

    /** @ORM\Column(type="json", name="menu") */
    private $menu;

    /**
     * @return mixed
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return RoutePermission
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * @param mixed $route
     * @return RoutePermission
     */
    public function setRoute($route)
    {
        $this->route = $route;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param mixed $role
     * @return RoutePermission
     */
    public function setRole($role)
    {
        $this->role = $role;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMenu()
    {
        return $this->menu;
    }

    /**
     * @param mixed $menu
     * @return RoutePermission
     */
    public function setMenu($menu)
    {
        $this->menu = $menu;
        return $this;
    }
}
