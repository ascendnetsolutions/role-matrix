<?php

namespace Ascend\RoleMatrix\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\MappedSuperclass()
 * @ORM\Table(name="role")
 * @UniqueEntity(fields={"name"})
 */
class Role
{
    const ROLE_ADMIN = 'ROLE_ADMIN';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(name="name", type="string")
     * @Assert\NotBlank()
     * @Assert\Regex(pattern="/^[A-Z_]+$/", message="app.ui.only_uppercase")
     */
    protected $name;

    /**
     * @ORM\Column(name="locked", type="boolean")
     */
    protected $locked = false;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Permission")
     */
    protected $permissions;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\RoutePermission", mappedBy="role", cascade={"remove"})
     */
    private $routePermission;

    public function __construct()
    {
        $this->permissions = new ArrayCollection();
    }

    /**s
     * @return mixed
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $name
     *
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return bool
     */
    public function isLocked(): bool
    {
        return $this->locked;
    }

    /**
     * @param bool $locked
     * @return Role
     */
    public function setLocked(bool $locked): self
    {
        $this->locked = $locked;

        return $this;
    }

    public function __toString(): string
    {
        return (string) $this->name;
    }

    /**
     * @return ArrayCollection
     */
    public function getPermissions()
    {
        return $this->permissions;
    }

    public function addPermission(RoutePermission $permission): self
    {
        if (!$this->permissions->contains($permission)) {
            $this->permissions[] = $permission;
        }

        return $this;
    }

    public function removePermission(RoutePermission $permission): self
    {
        if ($this->permissions->contains($permission)) {
            $this->permissions->removeElement($permission);
        }

        return $this;
    }


}