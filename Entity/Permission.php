<?php

namespace Ascend\RoleMatrix\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\MappedSuperclass()
 * @UniqueEntity(fields={"action"}, message="app.ui.permission_unique")
 */
class Permission
{
    const VIEW = 'view_permission';
    const EDIT = 'edit';
    const DELETE = 'delete';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", name="action")
     * @Assert\NotBlank()
     */
    private ?string $action;

    /**
     * @ORM\Column(type="string", name="name")
     ** @Assert\NotBlank()
     */
    private ?string $name;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAction(): ?string
    {
        return $this->action;
    }

    public function setAction($action): self
    {
        $this->action = $action;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName($name): self
    {
        $this->name = $name;
        return $this;
    }

    public function __toString()
    {
        return (string)$this->getName();
    }
}
