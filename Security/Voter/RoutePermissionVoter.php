<?php

namespace Ascend\RoleMatrix\Security\Voter;

use Ascend\RoleMatrix\Entity\RoutePermission;
use Ascend\RoleMatrix\Repository\IRoutePermissionRepository;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class RoutePermissionVoter extends Voter
{
    private $repository;

    public function __construct(IRoutePermissionRepository $repository)
    {
        $this->repository = $repository;
    }

    public function supports($attribute, $subject): bool
    {
        return in_array($attribute, [
            RoutePermission::VIEW,
            RoutePermission::EDIT,
            RoutePermission::DELETE
        ]);
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        if (!$user instanceof UserInterface) {
            return false;
        }

        if (in_array($this->getAdminRole(), $user->getRoles(), true)) {
            return true;
        }

        $permissions = $this->repository->findPermissionForRole(
            $user->getRoles()
        );

        if (is_null($permissions)) {
            return false;
        }

        /** @var RoutePermission $permission */
        foreach ($permissions as $permission) {
            foreach ($permission->getRoute() as $route) {
                if ($route != $subject) {
                    continue;
                }
                return true;
            }
        }

        return false;
    }

    protected function getAdminRole(): string
    {
        return getenv('ROLE_MATRIX_ADMIN');
    }
}
