<?php

namespace Ascend\RoleMatrix\Security\Voter;


use Ascend\RoleMatrix\Entity\Permission;
use Ascend\RoleMatrix\Entity\Role;
use Ascend\RoleMatrix\Repository\IRoleRepository;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class PermissionVoter extends Voter
{
    private $repository;

    public function __construct(IRoleRepository $repository)
    {
        $this->repository = $repository;
    }

    protected function supports($attribute, $subject): bool
    {
        return in_array($attribute, [
            Permission::VIEW,
            Permission::EDIT,
            Permission::DELETE
        ]);
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        if (!$user instanceof UserInterface) {
            return false;
        }

        if (in_array($this->getAdminRole(), $user->getRoles(), true)) {
            return true;
        }

        $roles = $this->repository->findBy([
            'name' => $user->getRoles()
        ]);

        if (is_null($roles)) {
            return false;
        }

        /** @var Role $role */
        foreach ($roles as $role) {
            /** @var Permission $permission */
            foreach ($role->getPermissions() as $permission) {
                if ($permission->getAction() == $subject) {
                    return true;
                }
            }
        }
        return false;
    }

    protected function getAdminRole(): string
    {
        return getenv('ROLE_MATRIX_ADMIN');
    }
}
